# Rust で多倍長演算

https://qiita.com/termoshtt/items/338549b9bf3aba6a5def

![screenshot](./screenshot2.png)

## 精度保証付き数値計算の必要性

精度保証付き数値計算ではない通常の数値計算だと誤差によって不都合な事象が生じてしまう例。

[Rump の例題](https://ja.wikipedia.org/wiki/%E7%B2%BE%E5%BA%A6%E4%BF%9D%E8%A8%BC%E4%BB%98%E3%81%8D%E6%95%B0%E5%80%A4%E8%A8%88%E7%AE%97#Rump%E3%81%AE%E4%BE%8B%E9%A1%8C)
