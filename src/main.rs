use rug::{ops::Pow, Float};

fn f200(val: f64) -> Float {
  Float::with_val(200, val)
}

/// 任意精度浮動小数点数
/// https://qiita.com/termoshtt/items/338549b9bf3aba6a5def
fn f(a: Float, b: Float) -> Float {
  let a2 = a.clone().pow(2);
  let b2 = b.clone().pow(2);
  let b4 = b.clone().pow(4);
  let b6 = b.clone().pow(6);
  let b8 = b.clone().pow(8);
  (f200(333.75) - &a2) * &b6
    + &a2 * (f200(11.0) * &a2 * &b2 - f200(121.0) * &b4 - f200(2.0))
    + f200(5.5) * &b8
    + a / (f200(2.0) * b)
}

use num::rational::BigRational;

fn br(val: f64) -> BigRational {
  BigRational::from_float(val).unwrap()
}

/// 有理数で計算
fn f2(a: BigRational, b: BigRational) -> BigRational {
  let a2 = a.pow(2);
  let b2 = b.pow(2);
  let b4 = b.pow(4);
  let b6 = b.pow(6);
  let b8 = b.pow(8);
  (br(333.75) - &a2) * &b6
    + &a2 * (br(11.0) * &a2 * &b2 - br(121.0) * &b4 - br(2.0))
    + br(5.5) * &b8
    + a / (br(2.0) * b)
}

use rug::Rational;

fn br2(val: f64) -> Rational {
  Rational::from_f64(val).unwrap()
}

/// 有理数で計算
fn f3(a: Rational, b: Rational) -> Rational {
  let a2 = a.clone().pow(2);
  let b2 = b.clone().pow(2);
  let b4 = b.clone().pow(4);
  let b6 = b.clone().pow(6);
  let b8 = b.clone().pow(8);
  (br2(333.75) - &a2) * &b6
    + &a2 * (br2(11.0) * &a2 * &b2 - br2(121.0) * &b4 - br2(2.0))
    + br2(5.5) * &b8
    + a / (br2(2.0) * b)
}

fn main() {
  println!("{:e}", f(f200(77617.0), f200(33096.0)));
  let num = f2(br(77617.0), br(33096.0));
  let (s1, numer) = num.numer().to_u32_digits();
  let (s2, denom) = num.denom().to_u32_digits();
  if 1 == numer.len() && 1 == denom.len() {
    let fnum = if s1 == s2 { 1. } else { -1. } * (numer[0] as f64) / (denom[0] as f64);
    println!("{:e} = {}", fnum, num);
  } else {
    println!("??? = {}", num);
  }
  let num = f3(br2(77617.0), br2(33096.0));
  let (numer, denom) = num.clone().into_numer_denom();
  let fnum = numer.to_i32_wrapping() as f64 / denom.to_i32_wrapping() as f64;
  // let fnum: f64 = fnum.into();
  println!("{:e} = {}", fnum, num);
}
